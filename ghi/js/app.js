function createCard(name, description, pictureUrl, startDate, endDate, venu) {

    return `
    <div class="col-sm-4 mb-1">
      <div class="card shadow-lg mb-5 bg-body-tertiary rounded ">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${venu}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer text-body-secondary">
          <p class="card-text">${startDate} - ${endDate}</p>
        </div>
      </div>

    </div>
    `;
}

function createAlert(e) {
return `
    <div class="alert alert-danger" role="alert">
       Something went wrong on our end! ${e}
    </div>
    `;
}

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            const alert = createAlert('Bad Response!')
            const alertLoc = document.querySelector('.row');
            alertLoc.innerHTML += alert;
            // Figure out what to do when the response is bad
        } else {
            const data = await response.json();

            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const title = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const startDate = new Date(details.conference.starts).toLocaleDateString();
                    const endDate = new Date(details.conference.ends).toLocaleDateString();
                    const venu = details.conference.location.name
                    const html = createCard(title, description, pictureUrl, startDate, endDate, venu);
                    const container = document.querySelector('.row');
                    container.innerHTML += html;

                }
            }

        }
    } catch (e) {
        console.error(e);
        const alert = createAlert(e);
        const alertLoc = document.querySelector(".row");
        alertLoc.innerHTML += alert;
        // Figure out what to do if an error is raised

    }

});
