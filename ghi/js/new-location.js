
window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/states/';

    try {
        const response = await fetch(url);

        if (Response.ok) {
            // Figure out what to do when the response is bad
            const data = await response.json();


            const selectTag = document.getElementById('state');
            for (let state of data.states) {
                const optionElement = document.createElement('option');
                optionElement.value = state.abbrevation;
                optionElement.innerHTML = state.name;
                selectTag.appendChild(optionElement);
                const formTag = document.getElementById('create-location-form');
                formTag.addEventListener('submit', event => {
                    event.preventDefault();

                    });

                }

} else {
                console.error('Failed to fetch states:', response.status, response.statusText);
            }
        } catch (error) {
            console.error('Error fetching states:', error);
        }

    });
