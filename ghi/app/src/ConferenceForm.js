import React, { useEffect, useState } from 'react';

function ConferenceForm() {
    const [locations, setLocations] = useState([]);
    const [name, setName] = useState('');
    const [starts, setStarts] = useState('');
    const [ends, setEnds] = useState('');
    const [description, setDescription] = useState('');
    const [max_presentations, setMax_presentations] = useState('');
    const [max_attendees, setMax_attendees] = useState('');
    const [location, setLocation] = useState('')



    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations)
            // console.log(data)
        }
    };

    useEffect(() => {
        fetchData();
    }, []);

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    };

    const handleStartChange = (event) => {
        const value = event.target.value;
        setStarts(value);
    };

    const handleEndChange = (event) => {
        const value = event.target.value;
        setEnds(value);
    };

    const handleDescriptionChange = (event) => {
        const value = event.target.value;
        setDescription(value);
    };

    const handleMax_presentationsChange = (event) => {
        const value = event.target.value;
        setMax_presentations(value);
    };

    const handleMax_attendeesChange = (event) => {
        const value = event.target.value;
        setMax_attendees(value);
    };

    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    };

    const handleSubmit = async (event) => {
        event.preventDefault();


        const data = {};
        data.location = location;
        data.name = name;
        data.starts = starts;
        data.ends = ends;
        data.description = description;
        data.max_attendees = max_attendees;
        data.max_presentations = max_presentations;

        // console.log(data);

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newConfrence = await response.json();
            // console.log(conferenceLocation);

            // Clear form fields
            setName('');
            setStarts('');
            setEnds('');
            setDescription('');
            setMax_presentations('');
            setMax_attendees('');
            setLocation('');
        }
    };

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create new Conference</h1>
                    <form onSubmit={handleSubmit} id="create-conference-form">
                        <div className="form-floating mb-3">
                            <input
                                onChange={handleNameChange}
                                value={name}
                                placeholder="Conference name"
                                required type="text"
                                name="name"
                                id="name"
                                className="form-control"
                            />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                onChange={handleStartChange}
                                value={starts}
                                placeholder="Start Date"
                                required type="date"
                                name="starts"
                                id="starts"
                                className="form-control"
                            />
                            <label htmlFor="start">Start Date</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                onChange={handleEndChange}
                                value={ends}
                                placeholder="End Date"
                                required type="date"
                                name="ends"
                                id="ends"
                                className="form-control"
                            />
                            <label htmlFor="end">End Date</label>
                        </div>
                        <div className="form-floating mb-3">
                            <textarea
                                onChange={handleDescriptionChange}
                                value={description}
                                placeholder="Description"
                                required
                                // rows={5}
                                style={{ height: '7em' }}
                                // cols={33}
                                name="description"
                                id="description"
                                className="form-control"
                            />
                            <label htmlFor="description">Description</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                onChange={handleMax_presentationsChange}
                                value={max_presentations}
                                placeholder="Max Presentations"
                                required type="number"
                                name="max_presentations"
                                id="max_presentations"
                                className="form-control"
                            />
                            <label htmlFor="max_presentations">Max Presentations</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                onChange={handleMax_attendeesChange}
                                value={max_attendees}
                                placeholder="Max Attendees"
                                required type="number"
                                name="max_attendees"
                                id="max_attendees"
                                className="form-control"
                            />
                            <label htmlFor="max_attendees">Max Attendees</label>
                        </div>
                        <div className="mb-3">
                            <select
                                className="form-select"
                                onChange={handleLocationChange}
                                value={location}
                                required
                                id="location"
                            >
                                <option disabled value="">
                            Choose Location
                            </option>

                                {locations.map((location) => {
                                    return (
                                        <option key={location.name} value={location.id}>
                                            {location.name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>


                </div>
            </div >
        </div >
    );
}

export default ConferenceForm;
